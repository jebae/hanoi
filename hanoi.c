#include <stdio.h>

int empty_seat(int num){
	switch(num) {
		case 1:
			return 2;	
		case 2:
			return 1;
		case 3:
			return 0;
	}
	printf ("Empty seat Error\n");
	return 0;
}

void move(int target, int from, int to, int *count){
	printf ("moved %d from %d to %d count : %d\n", target, from, to, *count);
	*count = *count + 1;
}

void hanoi(int bottom, int from, int to, int *count){
	if (bottom == 1) {
		move(bottom, from, to, count);
	} else {
		int empty = empty_seat(from + to);
		hanoi(bottom - 1, from, empty, count);
		move(bottom, from, to, count);
		hanoi(bottom - 1, empty, to, count);
	}
}

int main() {
	int base, count=0;

	printf ("Number of base?");
	scanf("%d", &base);
	hanoi(base, 0, 2, &count);

	printf ("count : %d\n", count);
	return 1;
}
